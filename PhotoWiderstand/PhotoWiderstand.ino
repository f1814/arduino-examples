
#include <FtduinoSimple.h>

int photoWiderstand = 0;

void setup() {
  Serial.begin(9600); // Wert ist für Funktion des FT-Duinos belanglos 
  while(!Serial);      // warte auf USB-Verbindung
}

void loop() {
  // Foto Widerstand liefert 0 für dunkel und 1 für hell
  photoWiderstand = ftduino.input_get(Ftduino::I2);
  Serial.println(ftduino.input_get(Ftduino::I2));

  if(photoWiderstand == 0) {
    ftduino.output_set(Ftduino::O1, Ftduino::HI);
    delay(1000);
  } else {
    ftduino.output_set(Ftduino::O1, Ftduino::LO);
    delay(1000);
  }
}
