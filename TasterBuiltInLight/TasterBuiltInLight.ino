/*
  Lichtschalter
*/

#include <FtduinoSimple.h>

#define EVENT_TIME  480    // 480us
uint8_t event[EVENT_TIME];
bool light = false;
int photoWiderstand = 0;

// die setup-Funktion wird einmal beim Start aufgerufen
void setup() {
  pinMode(LED_BUILTIN, OUTPUT); //initialisiere LED
  Serial.begin(9600); // Wert ist für Funktion des FT-Duinos belanglos 
  while(!Serial);      // warte auf USB-Verbindung
  
}

// die loop-Funktion wird immer wieder aufgerufen
void loop() {
  // Warte bis Taster gedrückt
  if(ftduino.input_get(Ftduino::I1)) {
    // setze den Schalter auf das Gegenteil des aktuellen Wertes
    light = !light;
    Serial.println(light);
    // Warte eine halbe Sekunde
    delay(500);
  }
  // Schalte das Licht an oder aus je nach Tasterstellung
  if (light == true){
    digitalWrite(LED_BUILTIN, HIGH);
    ftduino.motor_set(Ftduino::M1, Ftduino::LEFT); // lässt einen Motor nach links drehen
    ftduino.output_set(Ftduino::O3, Ftduino::HI); // lässt eine Lampe leuchte
  }
  else {
    digitalWrite(LED_BUILTIN, LOW);
    ftduino.motor_set(Ftduino::M1, Ftduino::OFF);
    ftduino.output_set(Ftduino::O3, Ftduino::LO);
  }
}
