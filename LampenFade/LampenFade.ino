#include <Ftduino.h>

void setup() {
    ftduino.init();
    ftduino.output_set(Ftduino::O1, Ftduino::HI, Ftduino::MAX);
    ftduino.output_set(Ftduino::O2, Ftduino::LO, Ftduino::MAX);
}

void loop() {
    ftduino.output_set(Ftduino::O1, Ftduino::HI, Ftduino::LO);
    delay(100);
    ftduino.output_set(Ftduino::O1, Ftduino::HI, Ftduino::MAX/4);
    delay(100);
    ftduino.output_set(Ftduino::O1, Ftduino::HI, Ftduino::MAX/2);
    delay(100);
    ftduino.output_set(Ftduino::O1, Ftduino::HI, Ftduino::MAX);
    delay(100);
    ftduino.output_set(Ftduino::O1, Ftduino::HI, Ftduino::MAX/2);
    delay(100);
}
