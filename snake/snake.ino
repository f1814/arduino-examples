#include <FtduinoSimple.h>

void setup() {
  Serial.begin(9600);
  while (!Serial);

  clearConsole();
  poscur(20, 20);
  Serial.print("Drücke einen Taster, um das Spiel zu starten");

  byte initialSnakeArr [20][2] = {{10, 10}, {10, 11}, {10, 12}, {10, 13}},

  drawSnake(0, initialSnakeArr, 4);

}

boolean start = false;
byte curFoodL = 25;
byte curFoodC = 30;
byte colStop = 100;
byte lineStop = 50;
int dir = 0;
String snake = "*";
byte snakeLen = 4;
byte snakeArr [20][2] = {{10, 10}, {10, 11}, {10, 12}, {10, 13}};
byte posChange = 0;
int prevDir = -1;

void clearConsole() {
  Serial.print( F( "\033[2J" ));
}

void drawFood() {
  curFoodL = 25;
  curFoodC = 30;
  poscur(curFoodL, curFoodC);
  Serial.println("x");
}

void poscur(byte line, byte col) {
  Serial.print( F( "\033[" ));
  Serial.print( line );
  Serial.print( F( ";" ));
  Serial.print( col );
  Serial.print( F( "H" ));
}

void moveRight(int pos) {
  snakeArr[pos][1] = snakeArr[pos][1] + 1;
}
void moveDown(int pos) {
  snakeArr[pos][0] = snakeArr[pos][0] + 1;
}
void moveLeft(int pos) {
  snakeArr[pos][1] = snakeArr[pos][1] - 1;
}
void moveUp(int pos) {
  snakeArr[pos][0] = snakeArr[pos][0] - 1;
}

void moveSnake() {
  for (int i = 0; i < snakeLen; i++) {

    if (dir == 0) {
      if (prevDir == 1 && snakeArr[i][0] < posChange) {
        moveDown(i);
      } else if (prevDir == 3 && snakeArr[i][0] > posChange) {
        moveUp(i);
      } else {
        moveRight(i);
      }
    }

    if (dir == 1) {
      if (prevDir == 0 && snakeArr[i][1] < posChange) {
        moveRight(i);
      } else if (prevDir == 2 && snakeArr[i][1] > posChange) {
        moveLeft(i);
      } else {
        moveDown(i);
      }
    }

    if (dir == 2) {
      if (prevDir == 1 && snakeArr[i][0] < posChange) {
        moveDown(i);
      } else if (prevDir == 3 && snakeArr[i][0] > posChange) {
        moveUp(i);
      } else {
        moveLeft(i);
      }
    }

    if (dir == 3) {
      if (prevDir == 0 && snakeArr[i][1] < posChange) {
        moveRight(i);
      } else if (prevDir == 2 && snakeArr[i][1] > posChange) {
        moveLeft(i);
      } else {
        moveUp(i);
      }
    }
    
  }
}

void drawSnake() {
  for (int i = 0; i < snakeLen; i++) {
    poscur(snakeArr[i][0], snakeArr[i][1]);
    Serial.print(snake);
  }
}

void setChangePosition() {
  prevDir = dir;
  if (dir % 2 == 0) {
    posChange = snakeArr[0][1];
  } else {
    posChange = snakeArr[0][0];
  }
}

void loop() {
  if (!start) {
    if (ftduino.input_get(Ftduino::I1) || ftduino.input_get(Ftduino::I2) ) {
      start = true;
      delay(300);
    }
    return;
  }

  if (ftduino.input_get(Ftduino::I1)) {
    setChangePosition();
    if (dir < 3) {
      dir++;
    } else {
      dir = 0;
    }
  }

  if (ftduino.input_get(Ftduino::I2)) {
    setChangePosition();
    if (dir > 0) {
      dir--;
    } else {
      dir = 3;
    }
  }

  clearConsole();
  drawFood();
  moveSnake();
  drawSnake();

  poscur(10, 80);
  Serial.print(prevDir);
  poscur(11, 80);
  Serial.println(dir);

  delay(300);
}

// Wenn Essen und Schlange auf selbser Position sind: Schlange um eines erweitern, Essen an neuer Position malen
// statt hoch und runter Laufrichtung der Schlange ändern
// Wenn max Anzahl erreicht ist entsprechende Position auf null setzen so, dass Schlange wieder von vorne anfängt
// Punkte anzeigen oben links
