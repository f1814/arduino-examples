#include <FtduinoSimple.h>

void setup() {
  ftduino.output_set(Ftduino::O1, Ftduino::HI);
  ftduino.output_set(Ftduino::O2, Ftduino::LO);
}

void loop() {
  if(ftduino.input_get(Ftduino::I1)){
    delay(3000);
    ftduino.output_set(Ftduino::O1, Ftduino::LO);
    ftduino.output_set(Ftduino::O2, Ftduino::HI);
    delay(10000);
    ftduino.output_set(Ftduino::O1, Ftduino::HI);
    ftduino.output_set(Ftduino::O2, Ftduino::LO);
  }
}
