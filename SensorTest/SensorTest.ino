// BlinkO1.ino
//
// Blinken einer Lamoe an Ausgang O1
//
// (c) 2018 by Till Harbaum <till@harbaum.org>

#include <FtduinoSimple.h>

void setup() {
  Serial.begin(9600); // Wert ist für Funktion des FT-Duinos belanglos 
  while(!Serial);      // warte auf USB-Verbindung
}

void loop() {
  Serial.println(ftduino.counter_get_state(Ftduino::C1));
  delay(1000);                       // warte eine Sekunde
}
