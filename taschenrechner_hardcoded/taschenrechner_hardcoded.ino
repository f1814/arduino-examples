/*
  Ein einfacher Taschenrechner
*/

#include <FtduinoSimple.h>

// die setup-Funktion wird einmal beim Start aufgerufen
void setup() {
  Serial.begin(9600);

  while(!Serial);      // warte auf USB-Verbindung

  Serial.println("Mein Tascherechner");
}
int a = 6;
int b = 2;

// die loop-Funktion wird immer wieder aufgerufen
void loop() {
  if(ftduino.input_get(Ftduino::I1)) {
    Serial.print(a+b); 
    delay(500);
  }
  if(ftduino.input_get(Ftduino::I2)) {
    Serial.print(a-b); 
    delay(500);
  }
  if(ftduino.input_get(Ftduino::I3)) {
    Serial.print(a*b); 
    delay(500);
  }  
  if(ftduino.input_get(Ftduino::I4)) {
    Serial.print(a/b); 
    delay(500);
  }
}
