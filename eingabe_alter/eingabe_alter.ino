
void setup() {
 Serial.begin(9600);
 Serial.println("Wie alt bist du?");
}

void loop() {
  // auf serielle Eingabe warten
  while (Serial.available() > 0)
  {
    // solange lesen, bis return \n eingegeben wurde
    String Eingabe = Serial.readStringUntil('\n');
    // Ausgabe
    Serial.println("Du bist " + Eingabe + " Jahre alt!");
  }
}
